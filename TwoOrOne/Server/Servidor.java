import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.List;
import java.io.PrintStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
public class Servidor {
 
   private int porta;
   private List<PrintStream> clientes;
   private HashMap<Integer,Perfil> user;
   private HashMap<PrintStream,Perfil> user_stream;
   private int contId = 0;
   private ConnectionSettings settings;
   private int play[] = new int[3];
   private HashMap<Integer,Integer> game;
   
   //params of connection
   private final int NUM_USERS = 3;
   
   public static void main(String[] args) throws IOException {
     // inicia o servidor
     new Servidor(12345).executa();
   }
   
   public Servidor (int porta) {
     this.porta = porta;
     this.clientes = new ArrayList<PrintStream>();
     this.user = new HashMap<>();
     this.game = new HashMap<>();
     this.user_stream = new HashMap<>();
     this.settings = new ConnectionSettings();
     
   }
     
   public void executa () throws IOException {
     ServerSocket servidor = new ServerSocket(this.porta);
     System.out.println("Porta 12345 aberta!");
     
     while (true) {
       // aceita um cliente
       if(settings.tryConnect()){
          Socket cliente = servidor.accept();
          Perfil perfil = new Perfil(cliente.getPort());
          this.user.put(cliente.getPort(),perfil);
          this.contId++;
          System.out.println("Nova conexao com o cliente - " + contId + " - " + cliente.getPort());
          // adiciona saida do cliente a lista
          PrintStream ps = new PrintStream(cliente.getOutputStream());
          this.clientes.add(ps);
          this.user_stream.put(ps,perfil);
          // cria tratador de cliente numa nova thread
          TrataCliente tc = new TrataCliente(cliente.getInputStream(), this, cliente);


          new Thread(tc).start();
       }else{
            servidor.close();
            throw new IOException("Connection refused");   
       }
     }
   }
   
   public void setRoundMessages(int message,int port){
       this.game.put(port,message); 
       Set<Integer> keys = game.keySet();
       
   }
   
   public ConnectionSettings getSettings(){
      return settings;
   }

   
   public int verifyWinner() {
      int[] plays = new int[3];
	int[] doors = new int[3];
      Set<Integer> keys = game.keySet();
      int i = 0;
      for (int key : keys){
				plays[i] = game.get(key);
				doors[i] = key;
            i++;
		}
      
   	if (plays[0] == plays[1] && plays[0] != plays[2])
   		return doors[2];
   	
   	if (plays[0] == plays[2] && plays[0] != plays[1])
   		return doors[1];
   	
   	if (plays[1] == plays[2] && plays[1] != plays[0])
   		return doors[0]; 
   	
   
   	return -1;

   }
   
   
   
   public void showMessages(){
      Set<Integer> keys = game.keySet();
	String mensagem = "";
      for (int key : keys){
				mensagem += user.get(key).getNickname() +">" + game.get(key)+"|";
		}

	distribuiMensagem("4-"+mensagem);
   }
   
   public int getNumberOfPlays(){
      System.out.println("Numero de jogadas da rodada: " + this.game.size());
      return this.game.size();
   }
   
   public boolean tryToPlay(){
      
      if(getNumberOfPlays() == getNumberOfPlayers()){



         return true;
      }
      return false;
   }
   
   public int getNumUsers(){
      return NUM_USERS;
   }
   
   public int getNumberOfPlayers(){
      return contId;
   }
   
   public void trySendCommand(String msg, int port){
      if(getNumberOfPlayers() == NUM_USERS){
         setRoundMessages(Integer.parseInt(msg),port);
         getNumberOfPlays();
         if(tryToPlay()){
	    if (verifyWinner() != -1) {
 System.out.println("Ganhou a rodada: " + user.get(verifyWinner()).getNickname());
	    distribuiMensagem("2-"+ user.get(verifyWinner()).getNickname());

		} else {
	    distribuiMensagem("5-empate");
}

            showMessages();
            this.game.clear();
           
         }else{
            System.out.println("Esperando o outro jogador...");
         }
         
         
      }else{
         System.out.println(contId);
         settings.connectionStatus(3);
      }
   }
   
   public void modifyNickname(int port, String nickname){
      Perfil p = user.get(port);
      p.setNickname(nickname);
      user.put(port,p);

	if (contId == 3) {
			 distribuiMensagem("3-start");
	 String mensagem = "";
Set<Integer> keys = user.keySet();
      for (int key : keys){
				mensagem += user.get(key).getNickname() +"|";
		}
distribuiMensagem("1-"+mensagem);
	}
   }
   
   public String getUser(int port){
      Perfil p = user.get(port);
      return p.getNickname();
   }
   
   public void showNewConnection(String user){
      for (PrintStream cliente : this.clientes) {
       //cliente.println("1-" + user_stream.get(cliente).getNickname());
     }
   }
   
   public void receiveMsg(String msg, int user){
      int messages[] = new int[contId];
   }
   

   public void distribuiMensagem(String msg) {
     // envia msg para todo mundo  
     for (PrintStream cliente : this.clientes) {
       cliente.println(msg);
     }
   }
   
}

