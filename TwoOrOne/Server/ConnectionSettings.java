class ConnectionSettings {
   
   private final int NUM_USERS;
   public final int ERR_MAX_USERS;
   public final int ERR_MIN_USERS;
   public final int ERR_FULL_USERS;
   private int connected;
   
   
   public ConnectionSettings(){
      this.connected = 0;
      this.NUM_USERS = 3;
      this.ERR_MAX_USERS = 0;
      this.ERR_MIN_USERS = 1;
      this.ERR_FULL_USERS = 2;
   }
   
   public int getConnected(){
   
      return this.connected;
   }
   
   public boolean closeConnection(){
      boolean con = false;
      if(this.connected >= 0){
         con = true;
         this.connected--;
      }else{
          connectionStatus(ERR_MIN_USERS);
      }
      return con;
   }
   
   public boolean tryConnect(){
      boolean con = false;
      if (this.connected < NUM_USERS){
         con = true;
         this.connected++;
      }else if(this.connected == NUM_USERS + 1){
         con = false;
         this.connected++;
         connectionStatus(ERR_FULL_USERS);
      }else{
         connectionStatus(ERR_MAX_USERS);
      }
      return con;
   }
   
   public void connectionStatus(int ERROR){
      
      switch(ERROR){
         case 0:
            System.out.println("Nao foi possivel entrar na sala. O numero maximo de jogadores foi atingido.");
            break;
         case 1:
            System.out.println("Internal server error - Fail to close connection.");
            break;
         case 2:
            System.out.println("The game has the full number of players.");
            break;         
         case 3:
            System.out.println("Waiting for more players...");
            break;
            
          
      } 
   }
 
}
