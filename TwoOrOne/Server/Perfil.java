class Perfil{

   private int port;
   private String nickname;
   
   
   public Perfil(int port){
      this.port = port;
      this.nickname = "user" + port;
   }
   
   public Perfil(int port, String nickname){
      this.port = port;
      this.nickname = nickname;
   }
   
   public void setNickname(String nickname){
      this.nickname = nickname;
   }
   
   public String getNickname(){
      return this.nickname;
   }


}