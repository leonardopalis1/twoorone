import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.List;
import java.io.PrintStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class TrataCliente implements Runnable {
 
   private InputStream cliente;
   private Servidor servidor;
   private Socket socket;
   
   private final int DOIS;
   private final int UM;
   
   public TrataCliente(InputStream cliente, Servidor servidor, Socket socket) {
     this.cliente = cliente;
     this.servidor = servidor;
     this.socket = socket;
     
     this.DOIS = 2;
     this.UM = 1;
   }
 
   public void run() {
     // quando chegar uma msg, distribui pra todos
     Scanner s = new Scanner(this.cliente);
     while (s.hasNextLine()) {
       String msg = s.nextLine();
       String command = msg.split(" ")[0];
       if(command.equals("CONF")){
         servidor.modifyNickname(socket.getPort(),msg.split(" ")[1]); 
       }else if(command.equals(Integer.toString(this.DOIS)) || command.equals(Integer.toString(this.UM))){             
          servidor.trySendCommand(msg,socket.getPort());    
       }else{
         System.out.println("Command not suported.");
       }
       
     }
     s.close();
   }
 }
 
