/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author marcelorsjr
 */
public class JokenpoView extends javax.swing.JFrame {

    private Cliente cliente;

    /**
     * Creates new form JoenpoView
     */
    public JokenpoView(String host, int door, String nick) {
        Cliente cliente = new Cliente(host, door, nick);
        this.cliente = cliente;
         initComponents();
         buttonOne.setEnabled(false);
         buttonTwo.setEnabled(false);
         objectPlayer.setVisible(false);
         objectPlayerTwo.setVisible(false);
         objectPlayerThree.setVisible(false);
         playerThree.setVisible(false);
         playerTwo.setVisible(false);
        new Thread(cliente).start();
    }

    class Cliente implements Runnable {

        private String host;
        private int porta;
        private String nick;
        public PrintStream saida;

        public Cliente(String host, int porta, String nick) {
            this.host = host;
            this.porta = porta;
            this.nick = nick;
        }

        public void run() {
            try {

                Socket cliente = new Socket(this.host, this.porta);
                System.out.println("O cliente se conectou ao servidor! - " + cliente.isConnected());
            
                objectPlayerTwo.setVisible(false);
                objectPlayerThree.setVisible(false);
                
                // thread para receber mensagens do servidor
                Recebedor r = new Recebedor(cliente.getInputStream());
                new Thread(r).start();

                // l� msgs do teclado e manda pro servidor
                Scanner teclado = new Scanner(System.in);
                saida = new PrintStream(cliente.getOutputStream());
                saida.println("CONF " + nick);
                while (teclado.hasNextLine()) {

                }
                saida.close();
                teclado.close();
                cliente.close();
            } catch (Exception error) {
                System.out.println("Connection refused with server");
            }

        }

    }

    class Recebedor implements Runnable {

        private InputStream servidor;

        public Recebedor(InputStream servidor) {
            this.servidor = servidor;
        }

        public void run() {
            // recebe msgs do servidor e imprime na tela
            Scanner s = new Scanner(this.servidor);
            while (s.hasNextLine()) {
                String mensagem[] = s.nextLine().split("-");
                if ("3".equals(mensagem[0])) {
                        buttonOne.setEnabled(true);
                        buttonTwo.setEnabled(true);
                        objectPlayer.setVisible(false);
                        objectPlayerTwo.setVisible(false);
                        objectPlayerThree.setVisible(false);
                        playerThree.setVisible(true);
                        playerTwo.setVisible(true);
                        labelWaitingPlayers.setVisible(true);
                }else if ("1".equals(mensagem[0])) {
                    System.out.println("MENSAGEM: "+mensagem[1]);
                     String users[] = mensagem[1].split("\\|");
                       System.out.println(users[0]);
                        labelWaitingPlayers.setVisible(false);
                        for (int i = 0; i < 3; i++) {
                             System.out.println(users[i]);
                            if (users[i] == null ? cliente.nick == null : users[i].equals(cliente.nick)){
                                labelPlayer.setText(users[i]);
                            } else {
                                if ("".equals(labelPlayerTwo.getText())) {
                                    labelPlayerTwo.setText(users[i]);
                                } else {
                                    labelPlayerThre.setText(users[i]);
                                }
                                       
                            }
                        }
                    
                } else if ("4".equals(mensagem[0])) {
                    String users[] = mensagem[1].split("\\|");
                        objectPlayerTwo.setVisible(true);
                        objectPlayerThree.setVisible(true);
                        
                    
                    for (int i = 0; i < 3; i++) {
                           String jogadas[] = users[i].split(">");
                           if (jogadas[0].equals(labelPlayer.getText())) {
                               if (jogadas[1].equals("1")) {
                                   objectPlayer.setIcon(new ImageIcon(getClass().getResource( "Up_Pointing_Hand_Emoji.png")));
                               } else {
                                   objectPlayer.setIcon(new ImageIcon(getClass().getResource( "Victory_Hand_Emoji.png")));
                               }
                           } else if (jogadas[0].equals(labelPlayerTwo.getText())){
                                   if (jogadas[1].equals("1")) {
                                   objectPlayerTwo.setIcon(new ImageIcon(getClass().getResource( "Up_Pointing_Hand_Emoji.png")));
                               } else {
                                   objectPlayerTwo.setIcon(new ImageIcon(getClass().getResource( "Victory_Hand_Emoji.png")));
                               }
                           } else if (jogadas[0].equals(labelPlayerThre.getText())){
                                                              if (jogadas[1].equals("1")) {
                                   objectPlayerThree.setIcon(new ImageIcon(getClass().getResource( "Up_Pointing_Hand_Emoji.png")));
                               } else {
                                   objectPlayerThree.setIcon(new ImageIcon(getClass().getResource( "Victory_Hand_Emoji.png")));
                               }
                           }
                     }
                }  else if ("2".equals(mensagem[0])) {
                     labelWaitingPlayers.setVisible(true);
                    labelWaitingPlayers.setText("VENCEDOR: "+mensagem[1]);
                }  else if ("5".equals(mensagem[0])) {
                     labelWaitingPlayers.setVisible(true);
                    labelWaitingPlayers.setText("EMPATE");
                }
                        
                

                System.out.println(mensagem[1]);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        playerTwo = new javax.swing.JLabel();
        playerThree = new javax.swing.JLabel();
        buttonOne = new javax.swing.JButton();
        buttonTwo = new javax.swing.JButton();
        objectPlayer = new javax.swing.JLabel();
        objectPlayerTwo = new javax.swing.JLabel();
        objectPlayerThree = new javax.swing.JLabel();
        labelWaitingPlayers = new javax.swing.JLabel();
        labelPlayerTwo = new javax.swing.JLabel();
        labelPlayerThre = new javax.swing.JLabel();
        labelPlayer = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(600, 400));
        setResizable(false);

        playerTwo.setIcon(new javax.swing.ImageIcon(getClass().getResource("missing.png"))); // NOI18N

        playerThree.setIcon(new javax.swing.ImageIcon(getClass().getResource("missing.png"))); // NOI18N

        buttonOne.setText("UM");
        buttonOne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonOneActionPerformed(evt);
            }
        });

        buttonTwo.setText("DOIS");
        buttonTwo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonTwoActionPerformed(evt);
            }
        });

        objectPlayer.setIcon(new javax.swing.ImageIcon(getClass().getResource("Up_Pointing_Hand_Emoji.png"))); // NOI18N

        objectPlayerTwo.setIcon(new javax.swing.ImageIcon(getClass().getResource("Up_Pointing_Hand_Emoji.png"))); // NOI18N

        objectPlayerThree.setIcon(new javax.swing.ImageIcon(getClass().getResource("Up_Pointing_Hand_Emoji.png"))); // NOI18N

        labelWaitingPlayers.setText("AGUARDANDO JOGADORES...");

        labelPlayerTwo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        labelPlayerThre.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        labelPlayer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(280, 280, 280)
                .addComponent(objectPlayer))
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(playerThree, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelPlayerTwo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(labelWaitingPlayers))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(objectPlayerTwo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 238, Short.MAX_VALUE)
                        .addComponent(objectPlayerThree)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(playerTwo)
                        .addGap(12, 12, 12))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelPlayerThre, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(238, 238, 238)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(labelPlayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonOne, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(buttonTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(playerThree)
                            .addComponent(playerTwo))
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelPlayerThre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelPlayerTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelWaitingPlayers)
                        .addGap(68, 68, 68)
                        .addComponent(objectPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(objectPlayerTwo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(objectPlayerThree, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonOne)
                    .addComponent(buttonTwo))
                .addGap(10, 10, 10)
                .addComponent(labelPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonOneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonOneActionPerformed
        // TODO add your handling code here:
        objectPlayer.setVisible(true);
        objectPlayer.setIcon(new ImageIcon(getClass().getResource( "Up_Pointing_Hand_Emoji.png")));
        cliente.saida.println("1");

    }//GEN-LAST:event_buttonOneActionPerformed

    private void buttonTwoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonTwoActionPerformed
        // TODO add your handling code here:
        objectPlayer.setVisible(true);
        objectPlayer.setIcon(new ImageIcon(getClass().getResource( "Victory_Hand_Emoji.png")));
        cliente.saida.println("2");
    }//GEN-LAST:event_buttonTwoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonOne;
    private javax.swing.JButton buttonTwo;
    private javax.swing.JLabel labelPlayer;
    private javax.swing.JLabel labelPlayerThre;
    private javax.swing.JLabel labelPlayerTwo;
    private javax.swing.JLabel labelWaitingPlayers;
    private javax.swing.JLabel objectPlayer;
    private javax.swing.JLabel objectPlayerThree;
    private javax.swing.JLabel objectPlayerTwo;
    private javax.swing.JLabel playerThree;
    private javax.swing.JLabel playerTwo;
    // End of variables declaration//GEN-END:variables
}
